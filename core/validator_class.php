<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 020 20.02.18
 * Time: 23:02
 */

abstract class Validator{
    const CODE_UNKNOWN = 'UNKNOWN_ERROR';

    protected $data;

    protected $errors = array();

    function __construct($data)
    {
        $this->data = $data;
        $this->validate();
    }

    abstract protected function validate();

    public function getErrors(){
        return $this->errors;
    }

    public function isValid(){
        return count($this->errors) == 0;
    }

    protected function setError($code){
        $this->errors[] = $code;
    }

    protected function isContainQuotes($data){
        $array = array("\"", "'", "`", "&quot;", "&apos;");
        foreach ($array as $value){
            if(strpos($data, $value) !== false){
                return true;
            }
        }
        return false;
    }
}