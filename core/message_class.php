<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 025 25.02.18
 * Time: 21:39
 */

class Message{
    public function __construct($file)
    {
        $this->data = parse_ini_file($file);
    }

    public function get($name){
        return $this->data[$name];
    }
}