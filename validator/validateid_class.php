<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 003 03.03.18
 * Time: 22:09
 */

class ValidateID extends Validator{

    protected function validate()
    {
        $data = $this->data;
        if(!is_null($data) && !is_int($data) || ($data<0)){
            $this->setError(self::CODE_UNKNOWN);
        }
    }
}