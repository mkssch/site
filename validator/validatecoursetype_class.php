<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 026 26.03.18
 * Time: 21:36
 */

class ValidateCourseType extends Validator{

    const MAX_COURSE_TYPE = 3;

    protected function validate()
    {
     $data = $this->data;
     if(!is_int($data)){
         $this->setError(self::CODE_UNKNOWN);
     } else {
         if(($data < 1) || $data > self::MAX_COURSE_TYPE){
             $this->setError(self::CODE_UNKNOWN);
         }
     }
    }
}