d<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 003 03.03.18
 * Time: 22:15
 */

class ValidateIMG extends Validator{

    protected function validate()
    {
        $data = $this->data;
        if(!is_null($data) && !preg_match("/^[a-z0-9-_]+\.(jpg|jpeg|gif|png)$/i", $data)){
            $this->setError(self::CODE_UNKNOWN);
        }
    }
}