<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 003 03.03.18
 * Time: 21:17
 */

class ValidateData extends Validator{

    protected function validate()
    {
        $data = $this->data;
        if(!is_null($data) && strtotime($data) === false){
            $this->setError(self::CODE_UNKNOWN);
        }

    }
}