<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 001 01.03.18
 * Time: 23:39
 */

class ValidateActivation extends Validator{

    const MAX_LEN = 100;

    protected function validate(){
     $data = $this->data;
     if(mb_strlen($data) > self::MAX_LEN){
        $this->setError(self::CODE_UNKNOWN);
     }
    }
}