<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 011 11.03.18
 * Time: 14:20
 */

class ValidateText extends Validator{

    const MAX_LEN = 5000;
    const CODE_EMPTY = 'ERROR_TEXT_EMPTY';
    const CODE_MAX_LEN = 'ERROR_MAX_LEN';

    protected function validate(){
     $data = $this->data;
     if(mb_strlen($data) == 0){
         $this->setError(self::CODE_EMPTY);
     }
     if(mb_strlen($data) > self::MAX_LEN){
         $this->setError(self::CODE_MAX_LEN);
     }
    }
}