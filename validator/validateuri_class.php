<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 011 11.03.18
 * Time: 14:52
 */

class ValidateURI extends Validator{

    const MAX_LEN = 255;

    protected function validate()
    {
     $data = $this->data;
     if(mb_strlen($data) > self::MAX_LEN){
         $this->setError(self::CODE_UNKNOWN);
     } else {
         $pattern = "~^(?:/[a-z0-9.,_@%&?+=\~/-]*)?(?:#[^ '\"&<>]*)?$~i";
         if(!preg_match($pattern, $data)){
             $this->setError(self::CODE_UNKNOWN);
         }
     }
    }

}