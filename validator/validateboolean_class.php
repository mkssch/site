<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 003 03.03.18
 * Time: 21:05
 */

class ValidateBoolean extends Validator{

    protected function validate()
    {
        $data = $this->data;
        if(($data != 0) && ($data != 1)){
            $this->setError(self::CODE_UNKNOWN);
        }
    }
}