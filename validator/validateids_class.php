<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 026 26.03.18
 * Time: 21:40
 */

class ValidateIDs extends Validator{


    protected function validate()
    {
        $data = $this->data;
       if(is_null($data)){
           return;
       }
       if(!preg_match("/^\d+(,\d+)*\d?$/", $data)){
           $this->setError(self::CODE_UNKNOWN);
       }
    }
}