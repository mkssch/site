<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 026 26.02.18
 * Time: 22:10
 */

class Mail extends AbstractMail{

    public function __construct()
    {
        parent::__construct(new View(Config::DIR_EMAIL), Config::ADM_EMAIL);
        $this->setFromName(Config::ADM_NAME);
    }

}