<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 007 07.01.18
 * Time: 13:34
 */
abstract class Config{

    const SITENAME = 'MyRusakov.ru';
    const SECRET = 'DGLJDG5';
    const ADDRES = 'http://mysile.local';
    const ADM_NAME = 'Михаил Русаков';
    const ADM_EMAIL = 'admin@myrusakov.ru';

    const DB_HOST = 'localhost';
    const DB_USER = 'root';
    const DB_PASSWORD = '';
    const DB_NAME = 'site';
    const DB_PREFIX = 'xyz_';
    const DB_SYM_QUERY = "?";

    const DIR_IMG = '/images';
    const DIR_IMG_ARTICLES = '/images/articles';
    const DIR_IMG_AVARTAR = '/images/avatar';
    const DIR_TMPL = 'C:/Server/OpenServer/domains/mysite/tmpl/';
    const DIR_EMAIL = 'C:/Server/OpenServer/domains/mysite/tmpl/email';

    const FILE_MESSAGES = 'C:/Server/OpenServer/domains/mysite/text/messages.ini';

    const FORMAT_DATE = "%d.%m.%Y %H:%M:%S";

    const COUNT_ARTICLES_ON_PAGE = 3;
    const COUNT_SHOW_PAGES = 10;

    const MIN_SEARCH_LEN = 3;
    const LEN_SEARCH_RESULT = 255;

    const DEFAULT_AVATAR = 'default.png';
    const MAX_SIZE_AVATAR = 51200;
}
?>