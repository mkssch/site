<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 026 26.02.18
 * Time: 22:20
 */

abstract class Module extends AbstractModule{

    public function __construct()
    {
        parent::__construct(new View(Config::DIR_TMPL));
    }

}