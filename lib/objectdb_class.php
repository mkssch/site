<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 026 26.02.18
 * Time: 21:59
 */

abstract class ObjectDB extends AbstractObjectDB{

    private static $months = array('янв', 'фев', 'март', 'апр', 'май', 'июнь', 'июль', 'авг', 'сен', 'окт', 'ноя', 'дек');

    public function __construct($table)
    {
        parent::__construct($table, Config::FORMAT_DATE);
    }

    protected static function getMonth($date = false){
        if($date){
            $date = strtotime($date);
        } else {
            $date = time();
        }
        return self::$months[date('n', $date)-1];
    }

    public function preEdit($field, $value){
        return true;
    }

    public function postEdit($field, $value){
        return true;
    }

}